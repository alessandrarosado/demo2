pipeline {
    agent any     
    environment { 
		EMAIL_RECIPIENTS = 'alessandra.rosado2308@gmail.com'
	}
    
        stages {

            //build
            stage('Build') {
                steps {
                            sh 'npm install'                            
                            sh 'npm install json-server' 
                            sh 'npm run build'
                    }
              }          

            //test unit
           stage('Tests Unitarios') {
                steps {
                    
                   sh 'npm install karma --save-dev'
                   sh 'npm install karma-jasmine karma-chrome-launcher jasmine-core --save-dev'
                   sh 'npm install karma-mocha-reporter --save-dev'                   
                   sh 'npm install karma-junit-reporter --save-dev --link'
                   sh 'npm uninstall karma-coverage-istanbul-report'
                   sh 'npm install karma karma-coverage'                    
                    }
            }
            stage('e2e tests') {
                steps {                    
                    sh 'npm install typescript --save-dev'
                    sh 'npm install protractor'                    
                    sh 'npm run e2e:ci'
                    sh 'npm run e2e'
                }
            }
            
            //sonar

            stage('SonarQube analysis') {            			
                steps {   

                    sh 'npm install sonar-scanner --save-dev'
                    sh 'npm run sonar-scanner'                   
                    /*withSonarQubeEnv('sonarqube-9.1') {
                     sh "${scannerHome}/bin/sonar-scanner"
                    }*/
                }
            }
            //Nexus
                stage('Zip artifact'){
                    steps{
                      sh  'zip -9 -r dist.zip dist'
                    }
                }
    
                stage('Upload to Sonatype Nexus'){
        	        steps {
            	        script {
            	            dir ("${env.WORKSPACE}"){
            	            nexusArtifactUploader artifacts:
            	            [[artifactId:'test-karma',
            	            classifier: 'debug',
            	            file: 'dist.zip',
            	            type: 'test-karma']],
            	            credentialsId: '2dc74b6d-a9bd-49aa-b8b2-abf4fac9e642',
            	            groupId: 'com.angular',
            	            nexusUrl: '158.69.62.15:8081',
            	            nexusVersion: 'nexus3',
            	            protocol: 'http',
            	            repository: 'maven-snapshots',
            	            version: '1.0-SNAPSHOT'
            	        
            	    }
        		}
                
    		}
        }

            

}
post {
        success {
            sendEmail("Successful");
			notifySlack("SUCCESS");
        }
        unstable {
            sendEmail("Unstable");
			notifySlack("UNSTABLE");
        }
        failure {
            sendEmail("Failed");
			notifySlack("FAILED");
        }
    }

}

def sendEmail(status) {
	BUILD_TRIGGER_BY = "${currentBuild.getBuildCauses()[0].shortDescription} / ${currentBuild.getBuildCauses()[0].userId}"
	echo "BUILD_TRIGGER_BY: ${BUILD_TRIGGER_BY}"
					   
    mail(
            to: "$EMAIL_RECIPIENTS",
            subject: "Build $BUILD_NUMBER - " + status + " (${currentBuild.fullDisplayName})",
            body: "Changes:\n " + getChangeString() + "\n\n Check console output at: $BUILD_URL/console" + "\n")
}

@NonCPS
def getChangeString() {
    MAX_MSG_LEN = 100
    def changeString = ""

    echo "Gathering SCM changes"
    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            truncated_msg = entry.msg.take(MAX_MSG_LEN)
            changeString += " - ${truncated_msg} [${entry.author}]\n"
        }
    }

    if (!changeString) {
        changeString = " - No new changes"
    }
    return changeString
}

def notifySlack(String buildStatus = "STARTED") {
    buildStatus = buildStatus ?: "SUCCESS"

    def color

    if (buildStatus == "STARTED") {
        colorNotify = "#D4DADF"
    } else if (buildStatus == "SUCCESS") {
        colorNotify = "#BDFFC3"
    } else if (buildStatus == "UNSTABLE") {
        colorNotify = "#FFFE89"
    } else {
        colorNotify = "#FF9FA1"
    }

    def msg = "${buildStatus}: ${env.JOB_NAME} #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"

    slackSend(color: colorNotify, message: msg)
}
